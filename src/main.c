/** @file main.c
 *  @brief Entry point for application.
 *
 *  This file contains the main function (application entry point)
 *  and the matrix-vector multiplication function.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

/* -- Includes -- */

/* system includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <string.h>
#include <time.h>

/* local includes */
#include "arraynn.h"
#include "matrix.h"
#include "myutils.h"
#include "vector.h"

/*
 * Define MIN and MAX macros. Can't seem to find
 * a library that has these.
 */
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


/** @brief Perform matrix-vector multiplication.
 *
 *  @param M the matrix being multiplied
 *  @param v the vector multiplying the matrix
 *  @param rows the dimension of the matrix
 *  @return the result of the matrix-vector multiplication
 */
Vectorf_T matrix_vector_product(char *filepath, Vectori_T v, int *metadata);


int world_rank; /*<! Global variable to store the rank of a process. */

int world_size; /*<! Global variable to store the number of processes. */


/** @brief Entry function for application.
 *
 *  @param argc number of arguments passed to application
 *  @param argv array of arguments passed to application
 *  @return exit success or failure represented by an integer
 */
int main(int argc, char **argv) {
	Vectori_T x = malloc(sizeof(Vectori_T));
	Vectorf_T y = malloc(sizeof(Vectori_T));
	int *metadata = calloc(4, sizeof(metadata));

	/*
	 * Initialize MPI.
	 */
	MPI_Init(&argc, &argv);

	/*
	 * Get rank and size and store them to global variables.
	 */
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	/*
	 * Read matrix and generate vector in the root process.
	 */
	if (world_rank == 0) {
		metadata = read_matrix_metadata(argv[1]);

		vectori_init(x, metadata[0]);
		vectori_random(x, 2);
	}

	/*
	 * Broadcast the dimension of the matrix to all processes.
	 * This value is needed to initialize arrays in matrix_vector_product.
	 */
	MPI_Bcast(metadata, 4, MPI_INT, 0, MPI_COMM_WORLD);

	/*
	 * Perform matrix-vector multiplication.
	 */
	y = matrix_vector_product(argv[1], x, metadata);

	/*
	 * Print result of matrix-vector multiplication.
	 */
	if (world_rank == 0) {
		printf("Mx = ");
		vectorf_print(y);
		printf("\n");
	}

	/*
	 * Free memory resources.
	 */
	if (world_rank == 0) {
		vectori_free(x);
	}

	//vectorf_free(y);
	free(metadata);

	/*
	 * Block and wait for all process before finalizing.
	 */
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	/*
	 * Exit with success.
	 */
	return EXIT_SUCCESS;
}

Vectorf_T
matrix_vector_product(char *filepath, Vectori_T v, int *metadata) {
	int k, start, startline, stop, x[metadata[0]];
	Vectorf_T y = malloc(sizeof(Vectorf_T));
	Vectorf_T ysum = malloc(sizeof(Vectorf_T));

	startline = 3;

	if (world_rank == 0) {
		for (k = 0; k < metadata[0]; k++) {
			x[k] = v->vector[k];
		}
	}
	MPI_Bcast(&x, metadata[0] , MPI_INT, 0, MPI_COMM_WORLD);

	vectorf_init(y, metadata[0]);
	int displacements[world_size];

	int max = 0;
	int rem = metadata[2];

	if (world_rank == 0) {
		for (k = world_size; k > 0; k--){
			displacements[(k - 1)] = rem - round(rem / k);
			rem -= round(rem / k);
			max = MAX(max, round(rem / k));
		}
	}

	/*
	 * Broadcast displacements so that processes know which lines to read
	 * from the matrix file.
	 */
	MPI_Bcast(&displacements, world_size, MPI_INT, 0, MPI_COMM_WORLD);

	/*
	 * Broadcast max value so that all processes can allocate receive buffers.
	 */
	MPI_Bcast(&max, 1, MPI_INT, 0, MPI_COMM_WORLD);

	float A[max];
	int I[max], J[max];

	/*
	 * Send payload to all processes.
	 */
	start = startline + displacements[world_rank];

	if (world_rank == (world_size - 1)) {
		stop = startline + (metadata[2] -1);
	}
	else {
		stop = startline + (displacements[world_rank + 1] - 1);
	}

	int count = 0;
	for (k = start; k <= stop; k++) {
		int i, j;
		float val;
		char line[256];

		read_file_at_index(filepath, k, line, 256);
		sscanf(line, "%d %d %f", &i, &j, &val);
		I[count] = i;
		J[count] = j;
		A[count] = val;
		count++;
	}

	/*
	 * Perform local calculations.
	 */
	for (k = 0; k < max; k++) {
		y->vector[I[k]] += A[k] * x[J[k]];
	}

	/*
	 * Reduce the calculated values.
	 */
    vectorf_init(ysum, metadata[0]);
	MPI_Reduce(y->vector, ysum->vector, metadata[0], MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

	/*
	 * Free memory resources allocated for global storage vectors.
	 */
	vectorf_free(y);

	/*
	 * Return result of matrix-vector multiplication.
	 */
	return ysum;
}
