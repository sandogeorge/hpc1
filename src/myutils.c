/** @file myutils.c
 *  @brief Implementations for miscellaneous utility functions used the application.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

/* -- Includes -- */

/* system includes */
#include <stdio.h>
#include <assert.h>

/* local includes */
#include "myutils.h"

bool
int_in_array(int val, int *a, int n) {
	int i;

	for (i = 0; i < n; i++) {
		if (a[i] == val) return true;
	}

	return false;
}

void
read_file_at_index(char *filepath, int idx, char *buf, int buflength) {
	FILE* fp;
	int count = 1;

	fp = fopen(filepath, "r");
	assert(fp != NULL);

	while (fgets(buf, buflength, fp) != NULL) {
		if (count == idx) {
			break;
		}
		else {
			count++;
		}
	}

	fclose(fp);
}
