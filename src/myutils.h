/** @file myutils.h
 *  @brief Prototypes for miscellaneous utility functions used the application.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

#ifndef MYUTILS
#define MYUTILS

/* -- Includes -- */

/* system includes */
#include <stdbool.h>


/** @brief Check if value exists in array.
 *
 *  @param val integer whose presence is checked for
 *  @param a array to check for presence of integer
 *  @param n size of a
 *  @return boolean value indicating if val is present in a
 */
extern bool int_in_array(int val, int *a, int n);

/** @brief Read a specific line from file
 *
 * 	@param filename string the file to read the line from
 * 	@param idx integer the index of the line to read (idx ∈ N)
 * 	@return string the required line
 */
extern void read_file_at_index(char *filepath, int idx, char *buf, int buflength);

#endif
