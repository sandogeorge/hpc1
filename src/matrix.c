/** @file matrix.c
 *  @brief Implementations of matrix related operations.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

/* -- Includes -- */

/* system includes */
#include <stdio.h>
#include <assert.h>

/* local includes */
#include "matrix.h"
#include "myutils.h"


void
read_matrix(char *filepath, ArrayNN_T a) {
	int i, j, zindex;
	float val;

	FILE *fp;

	fp = fopen(filepath, "r");
	assert(fp != NULL);

	fscanf(fp, "%d %d %f %d", &i, &j, &val, &zindex);
	fscanf(fp, "%d %d %f %d", &i, &j, &val, &zindex);
	while (fscanf(fp, "%d %d %f", &i, &j, &val) != EOF) {
		array_insert(a, (zindex == 0 ? i : (i - 1)), ( zindex == 0 ? j: (j - 1)), val);
	}

	fclose(fp);
}

int *
read_matrix_metadata(char *filepath) {
	int *data = calloc(4, sizeof(data));
	char line[256];

	read_file_at_index(filepath, 2, line, 256);
	sscanf(line, "%d %d %d", &data[0], &data[1], &data[2]);

	return data;
}

void
matrix_print(ArrayNN_T M) {
	int i, j;

	for (i = 0; i < M->dim; i++) {
		printf("[");
		for (j = 0; j < M->dim; j++) {
			printf("%e", M->array[i][j]);
			if (j != (M->dim - 1)) printf(", ");
		}
		printf("]\n");
	}
}
