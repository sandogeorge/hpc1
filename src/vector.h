/** @file vector.h
 *  @brief Prototypes for dynamic vector ADT.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

#ifndef VECTOR
#define VECTOR

/* -- Includes -- */

/* system includes */
#include <stdlib.h>

typedef struct Vectori {
	int *vector;
	size_t used;
	size_t n;
} *Vectori_T;

typedef struct Vectorf {
	float *vector;
	size_t used;
	size_t n;
} *Vectorf_T;

/** @brief Create n sized integer vector.
 *
 *  @param v vector to be initialized
 *  @param n dimension of v
 *  @return void
 */
extern void vectori_init(Vectori_T v, int n);

/** @brief Create n sized float vector.
 *
 *  @param v vector to be initialized
 *  @param n dimension of v
 *  @return void
 */
extern void vectorf_init(Vectorf_T v, int n);

/** @brief Insert value into integer vector.
 *
 *  @param v vector to be updated
 *  @param val value to be inserted into v
 *  return void
 */
extern void vectori_insert(Vectori_T v, int val);

/** @brief Insert value into float vector.
 *
 *  @param v vector to be updated
 *  @param val value to be inserted into v
 *  return void
 */
extern void vectorf_insert(Vectorf_T v, int val);

/** @brief Populate an  integer vector with random numbers from 0 to max.
 *
 *  @param v vector to be populated with random values
 *  @param max the maximum value for random integers
 *  @return void
 */
extern void vectori_random(Vectori_T v, int max);

/** @brief Populate an  float vector with random numbers from 0 to max.
 *
 *  @param v vector to be populated with random values
 *  @param max the maximum value for random integers
 *  @return void
 */
extern void vectorf_random(Vectorf_T v, int max);

/** @brief Print integer vector in the format [v1, v2,..., vn].
 *
 *  @param v vector to be printed
 *  @return void
 */
extern void vectori_print(Vectori_T v);

/** @brief Print float vector in the format [v1, v2,..., vn].
 *
 *  @param v vector to be printed
 *  @return void
 */
extern void vectorf_print(Vectorf_T v);

/** @brief Free resources allocated for  integer vector.
 *
 *  @param v vector for which memory resources should be freed
 *  @return void
 */
extern void vectori_free(Vectori_T v);

/** @brief Free resources allocated for  float vector.
 *
 *  @param v vector for which memory resources should be freed
 *  @return void
 */
extern void vectorf_free(Vectorf_T v);

#endif
