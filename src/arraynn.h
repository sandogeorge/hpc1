/** @file arraynn.h
 *  @brief Prototypes for dynamic multidimensional array ADT.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

#ifndef ARRAYNN
#define ARRAYNN

/* -- Includes -- */

/* system includes */
#include <stdlib.h>


typedef struct ArrayNN {
	float **array;
	size_t dim;
	size_t nnz;
} *ArrayNN_T;

/** @brief Create init_dim x init_dim array.
 *
 *  @param a array to be initialized.
 *  @param init_dim the dimension that the array should be initialized with
 *  @return void
 */
extern void array_init(ArrayNN_T a, size_t init_dim);

/** @brief Insert new value into array, expanding if necessary.
 *
 *  @param a array to be updated
 *  @param i row index of the value to be inserted
 *  @param j column index of the value to be inserted
 *  @return void
 */
extern void array_insert(ArrayNN_T a, int i, int j, float val);

/** @brief Free resources allocated for array.
 *
 *  @param a array for which memory resources should be freed
 *  @return void
 */
extern void array_free(ArrayNN_T a);

#endif
