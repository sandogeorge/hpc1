/** @file matrix.h
 *  @brief Function prototypes for matrix related operations.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

#ifndef MATRIX
#define MATRIX

/* -- Includes -- */

/* local includes */
#include "arraynn.h"


/** @brief Read matrix from a file.
 *
 *  @param filepath path to the file where the matrix is described
 *  @param a multidimensional array to store the matrix
 *  return void
 */
extern void read_matrix(char *filepath, ArrayNN_T a);

/** @brief Check the dimension of a matrix in a file.
 *
 *  @param filepath path to the file where the matrix is described
 *  @return the dimension of the matrix
 */
extern int *read_matrix_metadata(char *filepath);

/** @brief Print a matrix row by row.
 *
 * 	@param M multidimensional array containing the matrix to be printed
 * 	@return void
 */
extern void matrix_print(ArrayNN_T M);

#endif
