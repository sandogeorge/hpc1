/** @file vector.c
 *  @brief Implementations for dynamic vector ADT.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

/* -- Includes -- */

/* system includes */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

/* local includes */
#include "vector.h"


void
vectori_init(Vectori_T v, int n) {
	v->vector = calloc(n, sizeof(v->vector));
	v->n = n;
	v->used = 0;
}

void
vectorf_init(Vectorf_T v, int n) {
	v->vector = calloc(n, sizeof(v->vector));
	v->n = n;
	v->used = 0;
}

void
vectori_insert(Vectori_T v, int val) {
	if (v->used == v->n) {
		v->n++;
		int *tmp = realloc(v->vector, v->n * sizeof(v->vector));
		assert(tmp != NULL);
		v->vector = tmp;
	}
	v->vector[v->used++] = val;
}

void
vectorf_insert(Vectorf_T v, int val) {
	if (v->used == v->n) {
		v->n++;
		float *tmp = realloc(v->vector, v->n * sizeof(v->vector));
		assert(tmp != NULL);
		v->vector = tmp;
	}
	v->vector[v->used++] = val;
}

void
vectori_random(Vectori_T v, int max) {
	int i;

	/*
	 * Ensure vector is empty.
	 */
	assert(v->used == 0);

	/*
	 * Seed random number generator
	 */
	srand(time(NULL));

	/*
	 * Generate random integers and store them to random_vals.
	 */
	for (i = 0; i < v->n; i++) {
		vectori_insert(v, (rand() % max));
	}
}

void
vectorf_random(Vectorf_T v, int max) {
	int i;

	/*
	 * Ensure vector is empty.
	 */
	assert(v->used == 0);

	/*
	 * Seed random number generator
	 */
	srand(time(NULL));

	/*
	 * Generate random integers and store them to random_vals.
	 */
	for (i = 0; i < v->n; i++) {
		vectorf_insert(v, ((float)rand()/(float)(RAND_MAX/max)));
	}
}

void
vectori_print(Vectori_T v) {
	int i;
	printf("[");
	for (i = 0; i < v->n; i++) {
		printf("%d", v->vector[i]);
		if (i != (v->n - 1)) printf(", ");
	}
	printf("]\n\n");
}

void
vectorf_print(Vectorf_T v) {
	int i;
	printf("[");
	for (i = 0; i < v->n; i++) {
		printf("%e", v->vector[i]);
		if (i != (v->n - 1)) printf(", ");
	}
	printf("]\n\n");
}

void
vectori_free(Vectori_T v) {
	free(v->vector);
	v->vector = NULL;
	v->used = v->n = 0;
}

void
vectorf_free(Vectorf_T v) {
	free(v->vector);
	v->vector = NULL;
	v->used = v->n = 0;
}
