/** @file arraynn.c
 *  @brief Implementations for dynamic multidimensional array ADT.
 *
 *  @author Sando George <georges@student.mini.pw.edu.pl>
 *  @bug No known bugs.
 */

/* -- Includes -- */

/* system includes */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* local includes */
#include "arraynn.h"


void
array_init(ArrayNN_T a, size_t init_dim) {
	int i;

	/*
	 * Allocate memory for array.
	 */
	a->array = calloc(init_dim, sizeof(a->array));
	assert(a->array != NULL);

	for (i = 0; i < init_dim; i++) {
		a->array[i] = calloc(init_dim, sizeof(a->array[i]));
		assert(a->array[i] != NULL);
	}

	/*
	 * Initialize dimension and number of none zeros.
	 */
	a->dim = init_dim;
	a->nnz = 0;
}

void
array_insert(ArrayNN_T a, int i, int j, float val) {
	if (a->dim == j) {
		a->dim++;
		float **tmp = realloc(a->array, a->dim * sizeof(a->array));
		assert(tmp != NULL);
		a->array = tmp;

		int k;
		for (k = 0; k < a->dim; k++) {
			float *temp = realloc(a->array[k], a->dim * sizeof(a->array[k]));
			assert(temp != NULL);
			a->array[k] = temp;
		}
	}
	a->array[i][j] = val;
	if (val != 0) a->nnz++;
}

void
array_free(ArrayNN_T a) {
	int i;
	for (i = 0; i < a->dim; i++) {
		free(a->array[i]);
		a->array[i] = NULL;
	}
	free(a->array);
	a->array = NULL;
	a->dim = 0;
	a->nnz = 0;
}
